# Maven Commands

#### Starting a new Maven project
```bash
mvn archetype:generate -DgroupId=com.mycompany.app -DartifactId=my-app
```

#### Starting a new Maven project (default mode)
```bash
mvn archetype:generate -DgroupId=com.mycompany.app -DartifactId=my-app -Dinteractive=false
```

#### Compiling command (generates target folder)
```bash
mvn compile
```

#### Packaging command (generates .jar file)
```bash
mvn compile
```

#### Cleaning target folder
```bash
mvn clean
```

#### Installing all dependencies
```bash
mvn install
```

#### Creating a profile
```bash
mvn -P profileName package
```

#### Change default POM file
```bash
mvn -f pomfile.xml package
```

#### Generate Documentation (only works with special pluggin)
```bash
mvn site
```

**[Maven Documentation](https://maven.apache.org/index.html)**
